#!/bin/sh

# Define the marker file path
MOSAIC_HOME="${CATALINA_HOME}/webapps/template"
MARKER_FILE="${MOSAIC_HOME}/.initialized"

# Wait for PostgreSQL to be ready
until pg_isready -h $PGHOST -p $PGPORT -U $PGUSER; do
  echo "Waiting for PostgreSQL..."
  sleep 2
done

# Check if the SQL script has already been run
if [ ! -f "${MARKER_FILE}" ]; then
  echo "Initializing database..."

  # Setup postgres role
  PGPASSWORD=$PGPASSWORD psql -h $PGHOST -U $PGUSER -d $PGDATABASE -c "CREATE ROLE postgres;"

  # Execute the SQL script
  PGPASSWORD=$PGPASSWORD psql -h $PGHOST -U $PGUSER -d $PGDATABASE -f ${MOSAIC_HOME}/template.sql
  
  # Check if psql command was successful
  if [ $? -eq 0 ]; then
    # Create the marker file to indicate that the SQL script has been run
    touch "${MARKER_FILE}"
    echo "Database initialization completed."
  else
    echo "Database initialization failed."
    exit 1
  fi
else
  echo "Database already initialized."
fi