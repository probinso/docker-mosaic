#!/bin/sh

# Stop all running containers
docker stop $(docker ps -aq)

# Remove all containers
docker rm $(docker ps -aq)

# Remove all images
docker rmi $(docker images -q)

# Remove all volumes
docker volume rm $(docker volume ls -q)

# Remove all networks
docker network rm $(docker network ls -q)

# Clear the build cache
docker builder prune -a -f

# Alternatively, use system prune to remove all unused data
docker system prune -a --volumes -f
