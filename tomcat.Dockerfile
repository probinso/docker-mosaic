FROM openjdk:17-alpine as base

# Set environment variables for Tomcat
ENV CATALINA_HOME /usr/local/tomcat
ENV ANT_HOME /usr/local/ant
ENV PATH ${CATALINA_HOME}/bin:${ANT_HOME}/bin:$PATH


# Install necessary packages
RUN apk --no-cache add wget tar postgresql-client gettext xmlstarlet emacs-nox bash tree

FROM base as tomcat
# Install Tomcat
ENV TCVER 10.1.24
RUN wget -O /tmp/tomcat.tar.gz https://archive.apache.org/dist/tomcat/tomcat-10/v${TCVER}/bin/apache-tomcat-${TCVER}.tar.gz
RUN tar xvzf /tmp/tomcat.tar.gz -C /usr/local
RUN ln -s /usr/local/apache-tomcat-${TCVER} ${CATALINA_HOME}

# Install Ant
ENV ANTVER 1.10.14
RUN wget -O /tmp/ant.tar.bz2 https://dlcdn.apache.org//ant/binaries/apache-ant-${ANTVER}-bin.tar.bz2
RUN tar -xjvf /tmp/ant.tar.bz2 -C /usr/local
RUN ln -s /usr/local/apache-ant-${ANTVER} ${ANT_HOME}

# Expose the default Tomcat port
EXPOSE 8080
EXPOSE 8000

# Copy the template and vendor files
COPY ./mosaic-template ${CATALINA_HOME}/webapps/template
COPY ./mosaic-vendor/*.jar ${CATALINA_HOME}/lib/

ARG TOMCATPASS
COPY ./tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
RUN xmlstarlet ed -P -S -L -u "/tomcat-users/user/@password" -v "${TOMCATPASS}" ${CATALINA_HOME}/conf/tomcat-users.xml


# Update context.xml
ARG PGUSER
ARG PGPASSWORD
ARG PGPORT
ARG PGHOST
ARG PGDATABASE

RUN xmlstarlet ed -P -S -L -u "/Context/Resource/@username" -v "${PGUSER}" ${CATALINA_HOME}/webapps/template/META-INF/context.xml
RUN xmlstarlet ed -P -S -L -u "/Context/Resource/@password" -v "${PGPASSWORD}" ${CATALINA_HOME}/webapps/template/META-INF/context.xml
RUN xmlstarlet ed -P -S -L -u "/Context/Resource/@url" -v "jdbc:postgresql://${PGHOST}:${PGPORT}/${PGDATABASE}" ${CATALINA_HOME}/webapps/template/META-INF/context.xml

# REMOVE ALL SECURITY?
RUN xmlstarlet ed -P -S -L -d "/Context/Valve" ${CATALINA_HOME}/webapps/manager/META-INF/context.xml
RUN xmlstarlet ed -P -S -L -d "/Context/Valve" ${CATALINA_HOME}/webapps/examples/META-INF/context.xml

# Copy the startup scripts
COPY init-psql.sh ${CATALINA_HOME}/bin/init-mosaic-psql.sh
RUN chmod +x ${CATALINA_HOME}/bin/init-mosaic-psql.sh

COPY start-tomcat.sh ${CATALINA_HOME}/bin/start-tomcat.sh
RUN chmod +x ${CATALINA_HOME}/bin/start-tomcat.sh

# Start Tomcat using the startup script
CMD ["start-tomcat.sh"]
