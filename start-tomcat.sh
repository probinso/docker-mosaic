#!/bin/sh

set -euo pipefail

# Initialize psql
init-mosaic-psql.sh

# Start Tomcat
catalina.sh run
